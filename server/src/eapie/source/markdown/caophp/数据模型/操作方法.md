<blockquote class="info"><p>操作数据库，支持链接多个不同地址的数据库。根据模型方法，可以安全的拼接SQL语句。</p></blockquote>

##### 全局赋值函数：

```
db([mixed $identify][,array $config][,array $construct_args][,bool $coverage][,closure $closure]);
```

##### 继承框架类：

```
framework\cao::db([mixed $identify][,array $config][,array $construct_args][,bool $coverage][,closure $closure]);
```


|参数类型 |	参数名称	|参数备注	|是否必须	|传参顺序|
|---|---|---|---|---|
|string/int/float|$identify|标识|	否|	mixed[0]|
|array|	$config	|自定义数据库配置|	否	|array[0]|
|array|	$construct_args|构造方法的参数|	否	|array[1]|
|bool	|$coverage|是否覆盖已存在标识|	否	|bool[0]|
|closure	|$closure|闭包函数|否|closure[0]|

- $identify 是创建数据库连接的标识，可以根据标识获取对象重复使用。
- $construct_args 是在第一次实例化的时候，给类的构造函数传入的参数，如array(参数1,参数2,...)，是一个索引数组。
- $coverage 为true覆盖则已存在标识，默认false则不覆盖。
- $closure 放入一个参数，如function($db){}，$db就是实例化后的对象。