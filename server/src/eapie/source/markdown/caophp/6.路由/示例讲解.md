#### 获取当前路由
参数为空返回当前完整路由，是一个字符串：
~~~
/* 继承框架类方式 $str_http = framework\cao::http(); */
$str_http = http();//返回的是字符串
~~~
获取当前路由的解析数据。传入闭包函数参数，可获得解析后的一个关联数组：
~~~
$arr_http = http(function($http){
	return $http;//返回的是一个关联数组。
});
~~~

* * * * *

#### 字符串路由生成与解析
解析路由字符串的方式：
~~~
http('/a/b/c.html?name=user&age=26', function($http){
	//打印数据
	printexit($http);
	});
~~~
~~~
/* ******************** 打印结果 ******************** */
//注意当前网站根目录在多级目录下：[SUBDIRECTORY_LINK] => /website
Array
(
	/* 路由的配置信息 */
    [config] => Array
        (
            [protocol] => 
            [version] => 
            [host] => 
            [suffix] => .html
            [delimiter] => /
            [index] => 1
            [path] => Array()
            [query] => Array()
            [delete] => Array()
            [anchor] => 
            [redirect] => Array()
            [assign] => Array()
        )
	
    /* 初步解析的路由碎片 */
    [parse_url] => Array
        (
            [path] => a/b/c
            [query] => name=user&age=26
        )
	
    /* 下面是解析后的完整数据 */
    
    //路径参数。是一个索引数组。
    [path] => Array
        (
            [0] => a
            [1] => b
            [2] => c
        )
	//GET参数。是一个关联数组。
    [query] => Array
        (
            [name] => user
            [age] => 26
        )
	//锚点
    [anchor] => 
    //协议
    [protocol] => http
    //主机和端口
    [host] => 127.0.0.1:8080
    //没有主机名和传输协议信息，相对路径路由
    [link] => /website/index.php/a/b/c.html?name=user&age=26
    //完整的路由信息，包含主机名和传输协议。
    [url] => http://127.0.0.1:8080/website/index.php/a/b/c.html?name=user&age=26
)
~~~


* * * * *

#### 当前路由生成与解析
也可以直接获取当前路由的解析数据，字符串参数直接为空即可：
~~~
http(function($http){
	//打印数据
	printexit($http);
	});
~~~

* * * * *

#### 自定义
传递一个数组，设置参数：
~~~
$config = array(
	'subdirectory_link' => '',//子目录链接为空
	'index' => 0,//隐藏入口文件
	'delete' => array(
		'path' => array(1),//删除索引为1的
		'query' => array('name'),
	),
	'query' => array('cs'=>'测试', 'cs2'=>'O&K'),
	'path' => array( 2 => 'xx'),//覆盖索引为2的
	'anchor' => '123456?name=user&gg=KO~',
	);
//这里如果不传入字符串参数，那么会自动以当前路由来进行解析处理
http('https://127.0.0.1:8080/a/b/c.html?name=user&age=26', $config, function($http){
	//打印数据
	printexit($http);
	});
~~~
~~~
/* ******************** 打印结果 ******************** */
Array
(
    [config] => Array( //...... )
    [parse_url] => Array( //...... )
    
    [path] => Array(
        [0] => a
        [2] => xx //索引为2被覆盖
        )

    [query] => Array(
        [age] => 26
        [cs] => 测试
        [cs2] => O&K
        )
	//锚点
    [anchor] => 123456?name=user&gg=KO~
    //协议会根据情况自动获取
    [protocol] => https
    [host] => 127.0.0.1:8080
    //已经隐藏了入口文件，并且子目录链接为空
    [link] => /a/xx.html?age=26&cs=%E6%B5%8B%E8%AF%95&cs2=O%26K#123456?name=user&gg=KO~
    [url] => https://127.0.0.1:8080/a/xx.html?age=26&cs=%E6%B5%8B%E8%AF%95&cs2=O%26K#123456?name=user&gg=KO~
)
~~~


* * * * *

#### 生成
生成一个自定义的URL
~~~
$config = array(
	'index' => 0,
	'delete' => array(
		//......
	),
	'query' => array(//......),
	//......
	);
 //自定义生成一个URL
$url = http('/a/b/c.html?name=user&age=26', $config);
//也可以根据当前路由，并修改某些参数生成一个URL
$url = http($config);
//闭包函数的方式
$url = http($config, function($http){
    //获取路由
    return $http['url'];
    });
~~~
