<blockquote class="info"><p>路由操作方法，传入不同的参数或者参数为空，那么得到不同的结果。参数为空，可以获取当前的路由，返回一个路由字符串。或者指定配置信息，传入闭包函数，获得操作解析并处理后的路由关联数组。也可以传入一个自定义的路由字符串进行解析操作。</p></blockquote>

##### 全局赋值函数：
~~~
http([string $url][,array $config][,closure $closure]);
~~~

##### 继承框架类：
~~~
framework\cao::http([string $url][,array $config][,closure $closure]);
~~~

| 参数类型  |  参数名称  |  参数备注  |  是否必须  |  传参顺序 |
| --- | --- | --- | --- | --- |
|  string  |  $url  |  URL字符串 |  否  |  string[0] |
|  array  |  $config  |  路由配置  |  否  | array[0] |
|  closure  |  $closure  |  闭包函数  |  否  | closure[0] |

- $url 可以是URL相对路径，也可以是带协议域名的完整链接。 
- $config 自定义当前操作的路由配置信息，与默认配置合并，自定义存在则覆盖默认。
- $closure 放入一个参数，如function($data){}，$data就是路由解析后的数据，是一个关联数组。