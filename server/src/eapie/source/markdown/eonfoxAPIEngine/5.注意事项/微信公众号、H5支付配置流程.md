## 在配置表（config）中，设置公众号账号信息、设置商户号信息
|  配置键   |  配置名称   | 格式   |参数详解   |
| --- | --- | --- | --- |
|   weixin_mp_access  |   微信公众号、H5网页支付配置  | JSON | {"id":"微信支付分配的公众账号ID（企业号corpid即为此appId）","state":"1"} |
|   weixin_pay_access |  微信支付商户配置  | JSON | {"pay_key":"支付密匙","mch_id":"微信支付分配的商户号","spbill_create_ip":"该IP同在商户平台设置的IP白名单中的IP没有关联，该IP可传用户端或者服务端的IP。","ssl_cert":"证书cert","ssl_key":"证书key","state":"为真表示开启，为假表示关闭"} |


## 进入微信商户平台的产品中心编辑"开发配置"
![输入图片说明](https://images.gitee.com/uploads/images/2019/0805/112516_f29a8799_632859.png "进入微信商户平台的产品中心编辑开发配置.png")
公众号支付与H5支付至少要配置如下四种域名：
```
//开发版接口请求地址
https://developer.eapie.eonfox.com/
//正式版接口请求地址
https://eapie.eonfox.com/

//下面两个是示例域名，是项目的站点域名
//正式版项目站点域名
http://emaiyx.com/ 
//测试版项目站点域名
http://mp.emshop.eonfox.com/
```
如下图所示：
![输入图片说明](https://images.gitee.com/uploads/images/2019/0805/112731_f95cf1c2_632859.png "配置域名.png")


## 进入微信商户平台的产品中心编辑“我授权的产品”
商户平台需要授权给公众号，如公众号支付需要JSAPI支付授权：
![输入图片说明](https://images.gitee.com/uploads/images/2019/0805/112852_59898445_632859.png "JSAPI支付授权.png")


## 进入微信商户平台的产品中心编辑“APPID授权管理”
![输入图片说明](https://images.gitee.com/uploads/images/2019/0805/113029_e306fcf5_632859.png "APPID授权管理.png")


## 进入微信公众平台的设置编辑“公众号设置”
需要在网页授权域名中 ，把接口域名添加上：
```
developer.eapie.eonfox.com
eapie.eonfox.com
```
![输入图片说明](https://images.gitee.com/uploads/images/2019/0805/113125_84779b33_632859.png "网页授权域名.png")

公众号还要关联商户号：
![输入图片说明](https://images.gitee.com/uploads/images/2019/0805/113159_ea90b1c6_632859.png "关联商户号.png")



